---
title: Encrypted E-mail with Cleartext Copies
docname: draft-dkg-mail-cleartext-copy-03
category: std
ipr: trust200902
consensus: yes
area: sec
workgroup: lamps
keyword: Internet-Draft
stand_alone: yes
submissionType: IETF
venue:
  group: "LAMPS"
  type: "Working Group"
  mail: "spasm@ietf.org"
  arch: "https://mailarchive.ietf.org/arch/browse/spasm/"
  repo: "https://gitlab.com/dkg/cleartext-copy"
  latest: "https://dkg.gitlab.io/cleartext-copy/"
author:
  -
    ins: D. K. Gillmor
    name: Daniel Kahn Gillmor
    email: dkg@fifthhorseman.net
  -
    ins: D. Huigens
    name: Daniel Huigens
    email: d.huigens@protonmail.com
    org: Proton AG
--- abstract

When an e-mail program wishes to send an encrypted message to multiple recipients, it is possible that it has no encryption capability for at least one of the recipients, for example because it cannot find a public key for those recipients.

In this circumstance, an e-mail program may choose to still send the message encrypted to the recipients it can encrypt to, and send the message in cleartext to the recipient it cannot encrypt to.

This draft defines an e-mail header to indicate to the recipients that received an encrypted copy, whether all recipients received an encrypted copy, or whether cleartext copies were sent, to inform them of the cryptographic protection status of the message.

--- middle

# Introduction

This document is concerned with end-to-end encrypted (E2EE) e-mail messages, regardless of the type of encryption used.
Both S/MIME ({{?RFC8551}}) and PGP/MIME ({{?RFC3156}}) standards support the creation and consumption of end-to-end encrypted e-mail messages.

When receiving end-to-end encrypted messages, the mail user agent (MUA) may want to display a visual security indicator to communicate to the user that the message was encrypted.
Similarly, the MUA may need to behave carefully not to leak the message contents in cleartext, for example when composing a reply-to-all to the message.
{{?I-D.ietf-lamps-e2e-mail-guidance}} offers guidance to MUAs for handling end-to-end encrypted messages.

However, when the message was originally sent in cleartext to some of the recipients, the message was effectively not end-to-end encrypted {{?I-D.knodel-e2ee-definition}} to all recipients, and these considerations don't apply.
For MUAs, it is thus important to know whether the message was end-to-end encrypted to all recipients.

This document defines the `E2EE-To-All` header, indicating whether the message was end-to-end encrypted to all (visible) recipients, and provides guidance for MUAs to generate and consume this header.

{::boilerplate bcp14-tagged}

## Terminology

In this draft:

- E2EE is short for End-to-End Encrypted, as defined by {{?I-D.knodel-e2ee-definition}}.

- MUA is short for Mail User Agent; an e-mail client.

- The visible participants of an e-mail message are the sender listed in the From header, and the recipients listed in the To and Cc headers. The recipients listed in the Bcc header are invisible participants.

# Scenarios

The following scenarios are examples where an encrypted message might be produced but some copies of the message are sent or stored in the clear.
In all scenarios, Alice is composing a new message to Bob and at least one other copy is generated.
Alice has a cryptographic key for Bob, and knows that Bob is capable of decrypting e-mail messages.

In each of the following scenarios, Alice's MUA generates an e-mail, and knows that there is at least one cleartext copy of the message stored on a system not under Alice's personal control.

## Simple Three-Party E-mail {#simple-three-party}

Alice sends a message to Bob and Carol, but Alice has no encryption-capable key for Carol.
She encrypts the copy to Bob, but sends a cleartext copy to Carol.

## Cleartext Remote Drafts Folder {#drafts}

Alice's MUA stores all draft copies of any message she writes in the clear in a Drafts folder, and that folder is itself stored on an IMAP server.
When she composes a message, the IMAP server has a cleartext copy of the draft, up until and including when she clicks "Send".
Her MUA instructs the IMAP server to delete the draft version of the message, but it also knows that it had at one point a cleartext copy, and cleartext might persist forever.
  
## Cleartext Remote Sent Folder {#sent}

Unlike in {{drafts}}, copies of messages sent to Alice's draft folder are encrypted or only stored locally.
But when sending an e-mail message to Bob, her MUA generates a cleartext copy an places it in her Sent folder, which is also stored on IMAP.

## Public Mailing List {#mailing-list}

Alice "Replies All" to message from Bob on a public mailing list.
The public mailing list has no encryption-capable public key (it is archived publicly in the clear), so Alice cannot encrypt to it.
But Alice's MUA is configured to opportunistically encrypt every copy possible, so the copy to Bob is encrypted.

## Trusted Mailserver {#trusted-mailserver}

Alice and Bob work in different organizations, and Alice's MUA has a policy of encrypting to outside peers that does not apply to members of her own organization.
Alice's co-worker David is in Cc on the message, and Alice and David both share a trusted mail server, so Alice does not feel the need to encrypt to David.
But she wants to defend against the possibility that Bob's mail server could read the contents of her message.
  
# Problems

Receiving MUA often needs to behave differently when handling a message with a different cryptographic status.

## Security Indicators

The MUA may want to indicate to the user whether received messages were end-to-end encrypted or not.
When a cleartext copy of the message was sent, the message was not end-to-end encrypted as defined by {{?I-D.knodel-e2ee-definition}}.
It is important that the message is not indicated as end-to-end encrypted by the MUA in that case, so that the user is away that other parties may have had access to the contents of the message.

## Reply All

When receiving an end-to-end encrypted message, a MUA needs to avoid leaking the contents of the message in the clear during a reply (see {{Section 5.4 of I-D.ietf-lamps-e2e-mail-guidance}}).

However, if one of the visible participants of the message does not have a cryptographic key, for example, neither the message nor any reply will be able to be end-to-end encrypted.
In this case, it is important to indicate that the original message was not end-to-end encrypted, such that the recipient's MUA does not need to warn the user about sending an unencrypted reply to an encrypted message.

# The E2EE-To-All Header Field {#e2ee-to-all}

To solve the issues described above, this document specifies a new e-mail header field with the name `E2EE-To-All`.
The only defined values of this field are `1` and `0`.
This header indicates whether the message was end-to-end encrypted to all visible recipients.

## Sending Encrypted Messages with the E2EE-To-All Header Field {#sending-e2ee-to-all}

A MUA that creates a message that is encrypted to all visible participants SHOULD add this header field with a value of `1` to each copy of the message.

A MUA that creates an encrypted message with a cleartext copy to one of the visible participants MUST add this header field with a value of `0` to each encrypted copy of the message.

## Receiving Encrypted Messages with the E2EE-To-All Header Field {#receiving-e2ee-to-all}

When receiving a message with an `E2EE-To-All: 0` header, the MUA MUST NOT consider the message end-to-end encrypted. It MUST NOT indicate to the user that the message was end-to-end encrypted, and SHOULD NOT indicate the message as being more secure than an unencrypted message.

When composing a reply-to-all to such a message, the MUA MAY send an unencrypted reply to some or all of the participants, and MAY quote the original message in cleartext replies.

When receiving an encrypted message without an `E2EE-To-All` header, it MAY indicate the message as being encrypted while use of the header is not widespread yet, but SHOULD indicate the message as being unencrypted once usage is widespread, or if the MUA has other reasons to believe the sender may have sent an unencrypted copy of the message.

When composing a reply-to-all to such a message, however, the MUA SHOULD consider the message end-to-end encrypted, and SHOULD send only encrypted replies, or omit the quoted text of the original message, as specified in {{Section 5.4 of I-D.ietf-lamps-e2e-mail-guidance}}.

# IANA Considerations

The IANA registry of [Message Headers](https://www.iana.org/assignments/message-headers/message-headers.xhtml) should be updated to add a row with Header Field Name `E2EE-To-All`, no template, protocol `mail`, status `standard`, and a reference to this document.

# Security Considerations

## Misrepresentations By Sender are Out of Scope {#misrepresentations}

This document describes security considerations between mutually-cooperating, end-to-end encryption-capable MUAs.
Either party could of course leak cleartext contents of any such message either deliberately or by accident.

In some cases, such as a `Bcc` scenario, the sending MUA is deliberately taking action on the sender's behalf that they do not want the (listed) recipient to know about.
Indicating to the listed recipient that a `Bcc`ed copy was emitted in the clear may violate the sender's expectations about what was done with the message.

This specification is not intended to detect fraud, misbehavior, or deliberate misrepresenation from one of the clients.

## Cryptographic Guarantees {#protected-headers}

For the proposed solutions that require a header field, that header field itself needs cryptographic protections, or an intervening mail transport agent could inject it to tamper with the apparent cryptographic status of the message.

For this reason, any header field involved in this must be provided with header protection, as described in {{?I-D.ietf-lamps-header-protection}}.

Additionally, since this is dealing with encrypted messages only, the sender may want to strip the `E2EE-To-All` header from the unencrypted headers before sending the message, to avoid indicating to a mail transport agent whether a cleartext copy of the message is available somewhere.

--- back

# Document History

## Changes from draft-dkg-mail-cleartext-copy-01 to draft-dkg-mail-cleartext-copy-02

Define the `E2EE-To-All` header, and provide guidance to MUAs for generating and handling it.
Remove the other options previously considered in this draft.

## Changes from draft-dkg-mail-cleartext-copy-00 to draft-dkg-mail-cleartext-copy-01

Added some discussion about user expectations.
